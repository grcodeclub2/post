<?php
// Διεύθυνση URL του αρχείου ZIP από το GitHub
$githubZipUrl = 'https://github.com/GrCodeClub/grcodeclub.github.io/archive/refs/heads/main.zip';

// Το όνομα του τοπικού αρχείου ZIP που θα κατεβάσουμε
$zipFilePath = 'repository.zip';

// Το όνομα του φακέλου στον οποίο θα εξαγάγουμε τα αρχεία
$extractFolderName = '.'; // Αντικαταστήστε αυτό με το επιθυμητό όνομα φακέλου

// Κατέβασμα του αρχείου ZIP
file_put_contents($zipFilePath, file_get_contents($githubZipUrl));

// Δημιουργήστε τον φάκελο στον οποίο θα εξαγάγετε τα αρχεία
if (!is_dir($extractFolderName)) {
    mkdir($extractFolderName, 0777, true);
}

// Ελέγξτε αν το αρχείο ZIP κατέβηκε επιτυχώς
if (file_exists($zipFilePath)) {
    // Δημιουργία αντικειμένου ZipArchive
    $zip = new ZipArchive();
    
    // Άνοιγμα του αρχείου ZIP
    if ($zip->open($zipFilePath) === true) {
        // Εξαγωγή των αρχείων στο επιθυμητό μονοπάτι (στον φάκελο με το καθορισμένο όνομα)
        $zip->extractTo($extractFolderName);
        
        // Κλείσιμο του αρχείου ZIP
        $zip->close();
        
        echo "Τα αρχεία εξήχθησαν με επιτυχία στο: " . $extractFolderName;
    } else {
        echo "Δεν μπόρεσα να ανοίξω το αρχείο ZIP.";
    }
} else {
    echo "Δεν μπόρεσα να κατεβάσω το αρχείο ZIP.";
}

// Μετά την εξαγωγή του περιεχομένου, διαγράψτε το αρχείο ZIP
unlink($zipFilePath);


// Ορίστε το μονοπάτι του φακέλου που θέλετε να διαγράψετε
$folderPath = 'bin';

// Συνάρτηση για διαγραφή φακέλου και όλου του περιεχομένου του
function deleteDirectory($dir) {
    if (!is_dir($dir)) {
        return;
    }

    // Εύρεση όλων των αρχείων και φακέλων στο συγκεκριμένο φάκελο
    $files = array_diff(scandir($dir), ['.', '..']);
    foreach ($files as $file) {
        $filePath = $dir . DIRECTORY_SEPARATOR . $file;
        if (is_dir($filePath)) {
            // Αν το στοιχείο είναι φάκελος, καλέστε αναδρομικά τη συνάρτηση
            deleteDirectory($filePath);
        } else {
            // Αν είναι αρχείο, διαγράψτε το
            unlink($filePath);
        }
    }

    // Τέλος, διαγράψτε τον ίδιο τον φάκελο
    rmdir($dir);
}

// Διαγράψτε τον φάκελο
deleteDirectory($folderPath);

// Έλεγχος εάν ο φάκελος έχει διαγραφεί επιτυχώς
if (!is_dir($folderPath)) {
    echo "Ο φάκελος '$folderPath' διαγράφηκε με επιτυχία.";
} else {
    echo "Δεν μπόρεσα να διαγράψω τον φάκελο '$folderPath'.";
}

rename("grcodeclub.github.io-main", "bin")


?>